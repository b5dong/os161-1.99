#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>
#include <opt-A1.h>

/* 
 * This simple default synchronization mechanism allows only vehicle at a time
 * into the intersection.   The intersectionSem is used as a a lock.
 * We use a semaphore rather than a lock so that this code will work even
 * before locks are implemented.
 */

/* 
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution.   Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables.   You are also free to 
 * declare other global variables if your solution requires them.
 */

/*
 * replace this with declarations of any synchronization and other variables you need here
 */
struct lock * mutex;
static int vehicles[4][4];
static struct cv *cvs[4];

/* 
 * The simulation driver will call this function once before starting
 * the simulation
 *
 * You can use it to initialize synchronization and other variables.
 * 
 */
void
intersection_sync_init(void){
    mutex = lock_create("mutex");
    
    if(mutex == NULL){
        panic("catmouse_sync_init: could not create mutex\n");
    }
    
    for (unsigned int i=0; i < 4; ++i) {
        cvs[i] = cv_create("create cv");
        if(cvs[i] == NULL) {
            panic("could not create cv\n");
        }
    }
    
    for (unsigned int i=0; i < 4; ++i) {
        for (unsigned int j=0; j < 4; ++j) {
            vehicles[i][j] = 0;
        }
    }
    return;
}

/* 
 * The simulation driver will call this function once after
 * the simulation has finished
 *
 * You can use it to clean up any synchronization and other variables.
 *
 */
void
intersection_sync_cleanup(void){
    if(mutex != NULL){
        lock_destroy(mutex);
        mutex = NULL;
    }
    for (unsigned int i=0; i < 4; ++i) {
        cv_destroy(cvs[i]);
    }
    for (unsigned int i=0; i < 4; ++i) {
        for (unsigned int j=0; j < 4; ++j) {
            vehicles[i][j] = 0;
        }
    }
}
bool is_right_turn(Direction origin, Direction destination) {
    if (((origin == west) && (destination == south)) || ((origin == south) && (destination == east)) ||
        ((origin == east) && (destination == north)) || ((origin == north) && (destination == west))) {
        return true;
    } else {
        return false;
    }
}

bool Cannot_enter(Direction origin, Direction destination) {
    for (unsigned int i=0; i<4; ++i) {
        for (unsigned int j=0; j<4; ++j) {
            if (vehicles[i][j] > 0) {
                if (origin == i && destination == j) {
                    continue;
                } else if (origin == j && destination == i){
                    continue;
                } else if(destination != j && is_right_turn(origin, destination)){
                    continue;
                }
                else {
                    return true;
                }
            }
        }
    }
    return false;
}

/*
 * The simulation driver will call this function each time a vehicle
 * tries to enter the intersection, before it enters.
 * This function should cause the calling simulation thread 
 * to block until it is OK for the vehicle to enter the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle is arriving
 *    * destination: the Direction in which the vehicle is trying to go
 *
 * return value: none
 */
void intersection_before_entry(Direction origin, Direction destination) {
    KASSERT(origin != destination);
    lock_acquire(mutex);
    
    while (Cannot_enter(origin, destination)){
        cv_wait(cvs[destination],mutex);
    }
    vehicles[origin][destination] += 1;
    
    lock_release(mutex);
}
/*
 * The simulation driver will call this function each time a vehicle
 * leaves the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle arrived
 *    * destination: the Direction in which the vehicle is going
 *
 * return value: none
 */
void intersection_after_exit(Direction origin, Direction destination){
    KASSERT(origin != destination);
    
    lock_acquire(mutex);
    vehicles[origin][destination] -= 1;
    if (vehicles[origin][destination] == 0) {
        for (unsigned int i = 0; i < 4; ++i) {
            cv_broadcast(cvs[i], mutex);
        }
    }
    lock_release(mutex);
}
    /*
     volatile unsigned int leftturn_org = 5; // 5 (no vehicle), 0 (N), 1 (S), 2(W), 3(E)
     volatile unsigned int leftturn_des = 5;
     volatile int leftturn_counter = 0;
     volatile unsigned int straight_org = 5;
     volatile unsigned int straight_des = 5;
     volatile int straight_counter = 0;
     
     volatile unsigned int des_N = 0;
     volatile int des_N_counter = 0;
     volatile unsigned int des_S = 0;
     volatile int des_S_counter = 0;
     volatile unsigned int des_E = 0;
     volatile int des_E_counter = 0;
     volatile unsigned int des_W = 0;
     volatile int des_W_counter = 0;
     static int *des_counter[4];
     */
    /*
     bool is_straight(Direction origin, Direction destination){
     if (((origin == west) && (destination == east)) ||
     ((origin == south) && (destination == north)) ||
     ((origin == east) && (destination == west)) ||
     ((origin == north) && (destination == south))) {
     return true;
     } else {
     return false;
     }
     }
     
     bool Cannot_enter(Direction origin, Direction destination){
     if (is_right_turn(origin, destination)){
     if (des_counter(destination) >= 0){
     return true;
     } else {
     return false;
     }
     }
     
     else if (is_straight(origin, destination)){                // if there has left turn and go straight vehicle in vertical direction, then has to wait
     if (leftturn_org != 5 || leftturn_des != 5 ){
     return true;
     } else if (straight_org == 5 && straight_des == 5){
     return false;
     } else if (straight_org == origin || straight_org == destination){
     return false;
     } else {
     return true;
     }
     }
     
     else {
     if (straight_org != 5 || straight_org != 5 ){
     return true;
     } else if (leftturn_org == 5 && leftturn_des == 5){
     return false;
     } else if (leftturn_org == origin){
     return false;
     } else {
     return true;
     }
     }
     return true;
     }*/
