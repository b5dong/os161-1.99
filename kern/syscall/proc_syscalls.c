#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <copyinout.h>
#include "opt-A2.h"

#if OPT_A2
volatile int PID_counter = __MIN_PID;

void forkentry(void *data1, unsigned long data2){
    (void) data2;
    
    struct trapframe * child_trapframe = (struct trapframe*) data1;
    enter_forked_process(child_trapframe);
};

pid_t generate_pid(struct proc *proc, int *err) {
    (void) proc;
    
    if (PID_counter < __MAX_PID) {
        PID_counter += 1;
        return PID_counter - 1;
    } else {                                                                        // if the process list is full
        *err = EMPROC;
        return -1;
    }
}

int sys_fork(struct trapframe *tf,pid_t *retval){
    struct proc *childProc = proc_create_runprogram(curproc->p_name);               // Init field and alloc memory for child process
    
    if(childProc == NULL) return ENOMEM;
    
    int err = 0;
    
    int child_pid = childProc->pid;
    
    if (child_pid <= 0){
        proc_destroy(childProc);
        return err;
    }
    
    err = as_copy(curproc->p_addrspace,&(childProc->p_addrspace));
    if(err){
        proc_destroy(childProc);
        return err;
    }
                                                                                    // Add child / parent relationship
    spinlock_acquire(&curproc->p_lock);
    curproc->childPids[curproc->num_of_child] = child_pid;
    curproc->num_of_child += 1;
    spinlock_release(&curproc->p_lock);
    
    struct trapframe * child_trapframe = NULL;
    child_trapframe = kmalloc(sizeof(struct trapframe));
    if(child_trapframe == NULL){
        err = ENOMEM;
        return err;
    }
    memcpy(child_trapframe, tf, sizeof(struct trapframe));
    
    err = thread_fork("process", childProc, forkentry, child_trapframe, 0);         // create new thread for process to run
    
    if(err){
        proc_destroy(childProc);
        return err;
    }
    *retval = child_pid;
    
    return 0;
}
#endif

/* stub handler for getpid() system call                */
int sys_getpid(pid_t *retval) {
#if OPT_A2
    *retval = curproc->pid;
    return(0);
#else
    return(0);
#endif
}

#if OPT_A2
int Get_exit_Status(pid_t pid, int options){
    KASSERT(Proc_table[pid - __MIN_PID] != NULL);
    KASSERT(Proc_table[pid - __MIN_PID]->Table_item_cv != NULL);
    
    int status = 0;
    int state = 0;
    int exitcode = 0;
    
    lock_acquire(table_lk);
    
    if (Proc_table[pid - __MIN_PID]->procStatus == __PROCALIVE ) {                  // If still ailve, wait
        if (options == WNOHANG) {
            lock_release(table_lk);
            return ECHILD;
        } else {
            cv_wait(Proc_table[pid - __MIN_PID]->Table_item_cv, table_lk);
        }
    }
    state = Proc_table[pid - __MIN_PID]->exitstate;
    exitcode = Proc_table[pid - __MIN_PID]->exitcode;
    
    exitcode = exit_table[pid - __MIN_PID];
    
    if (state == __WSIGNALED) status = _MKWAIT_SIG(exitcode);
    else if (state == __WCORED) status = _MKWAIT_CORE(exitcode);
    else if (state == __WSTOPPED) status = _MKWAIT_STOP(exitcode);
    else status = _MKWAIT_EXIT(exitcode);
    
    lock_release(table_lk);
    
    return status;
}
#endif
/* stub handler for waitpid() system call                */
int sys_waitpid(pid_t pid, userptr_t status, int options, pid_t *retval) {
    int exitstatus;
    int result;
#if OPT_A2
    if (options != 0) {
        return EINVAL;
    }
    if (pid < 0) {
        return ESRCH;
    }
    
    bool is_my_child = false;                                                       // Check if pid is current process's child 's pid
    for (int i = 0; i < curproc->num_of_child; i ++){
        if (pid == curproc->childPids[i]) {
            is_my_child = true;
            break;
        }
    }
    if(is_my_child == false){
        * retval = ECHILD;
        return -1;
    }
    
    if (Proc_table[pid - __MIN_PID] == NULL){                                       // Check if current process exit
        * retval = ESRCH;
        return -1;
    }
    
    exitstatus = Get_exit_Status(pid, options);
    
    result = copyout((void *)&exitstatus,status,sizeof(int));
    if (result) {
        return(result);
    }
    *retval = pid;
    return(0);
    
#else
    exitstatus = 0;
    result = copyout((void *)&exitstatus,status,sizeof(int));
    if (result) {
        return(result);
    }
    *retval = pid;
    return(0);
#endif
}


/* this implementation of sys__exit does not do anything with the exit code */
/* this needs to be fixed to get exit() and waitpid() working properly */
void sys__exit(int exitcode) {
    struct addrspace *as;
    struct proc *p = curproc;
    /* for now, just include this to keep the compiler from complaining about
     an unused variable */
    (void)exitcode;
    
    DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);
    
    KASSERT(curproc->p_addrspace != NULL);
#if OPT_A2
    lock_acquire(table_lk);                                                         // Update current process's exit info and wake waiting porcess
    
    curproc->procStatus = __PROCEXITED;
    curproc->exitcode = exitcode;
    curproc->exitstate = __WEXITED;
    
    exit_table[curproc->pid - __MIN_PID] = exitcode;
    cv_broadcast(curproc->Table_item_cv, table_lk);
    
    lock_release(table_lk);
#endif
    as_deactivate();
    /*
     * clear p_addrspace before calling as_destroy. Otherwise if
     * as_destroy sleeps (which is quite possible) when we
     * come back we'll be calling as_activate on a
     * half-destroyed address space. This tends to be
     * messily fatal.
     */
    as = curproc_setas(NULL);
    as_destroy(as);
    
    /* detach this thread from its process */
    /* note: curproc cannot be used after this call */
    proc_remthread(curthread);
    
    /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
    proc_destroy(p);
    
    thread_exit();
    /* thread_exit() does not return, so we should never get here */
    panic("return from thread_exit in sys_exit\n");
}


#if OPT_A2
int sys_execv(char *progname, char **args) {
/* Do the same thing as runprogram */
    struct addrspace *as;
    struct vnode *v;
    vaddr_t entrypoint, stackptr;
    int result;
    
/* Step 1: Count the number of arguments and copy them into the kernel */
    /* Count the number of arguments */
    int args_counter = 0;
    while(args[args_counter] != NULL) ++ args_counter;

    /* Copy arguments into the kernel */
    char ** args_to_kernel = (char **) kmalloc((args_counter + 1) * sizeof(char *));
    if(args_to_kernel == NULL) return ENOMEM;
    
    for(int i = 0; i < args_counter ; i ++){
        size_t len = strlen(args[i]) + 1;
        args_to_kernel[i] = (char *) kmalloc(len * sizeof(char));
        
        if(args_to_kernel[i] == NULL){
            for(int j = 0; j < i; j ++ ) {
                kfree(args_to_kernel[j]);
            }
            kfree(args_to_kernel);
            return ENOMEM;
        }
        /* copy into kernel */
        result = copyin((const_userptr_t)args[i], args_to_kernel[i], len * sizeof(char));//
        if(result){
            for(int j = 0; j <= i; ++j) kfree(args_to_kernel[j]);
            kfree(args_to_kernel);
            return result;
        }
    }
    args_to_kernel[args_counter] = NULL;
    
/* Step 2: Copy the program path into the kernel */
    size_t progname_len = strlen(progname) + 1;
    size_t progname_kernel_size = progname_len * sizeof(char);
    
    char * progname_to_kernel =  (char *) kmalloc(progname_kernel_size);
    if(progname_to_kernel== NULL) {
        for(int i = 0; i < args_counter; i ++ ) {
            kfree(args_to_kernel[i]);
        }
        kfree(args_to_kernel);
        return ENOMEM;
    }
    /* copy into kernel */
    result = copyin((const_userptr_t)progname, (void *) progname_to_kernel, progname_kernel_size);
    if(result) {
        for(int i = 0; i < args_counter; i ++ ) {
            kfree(args_to_kernel[i]);
        }
        kfree(args_to_kernel);
        kfree(progname_to_kernel);
        return result;
    }
    
/* Step 3: Open the program file using vfs_open */
    /* Open the file. */
    result = vfs_open(progname_to_kernel, O_RDONLY, 0, &v);
    if (result) {
        for(int i = 0; i < args_counter; i ++ ) {
            kfree(args_to_kernel[i]);
        }
        kfree(args_to_kernel);
        kfree(progname_to_kernel);
        return result;
    }
    
/* Step 4: Create new address space, set process to the new address space, and activate it */
    /* Create a new address space. */
    
    as = as_create();
    if (as == NULL) {
        for(int i = 0; i < args_counter; i ++ ) {
            kfree(args_to_kernel[i]);
        }
        kfree(args_to_kernel);
        kfree(progname_to_kernel);
        vfs_close(v);
        return ENOMEM;
    }
    /* Switch to it and activate it. */
    struct addrspace * old_address_space = curproc_setas(as);
    as_activate();
    
/* Step 5: Using the opened program file, load the program image using load_elf */
    
    /* Load the executable. */
    result = load_elf(v, &entrypoint);
    if (result) {
        for(int i = 0; i < args_counter; i ++ ) {
            kfree(args_to_kernel[i]);
        }
        kfree(args_to_kernel);
        kfree(progname_to_kernel);
        vfs_close(v);
        return result;
    }
    /* Done with the file now. */
    vfs_close(v);
    kfree(progname_to_kernel);
    
/* Step 6: Need to copy the arguments into the new address space.(both the array and the strings) */
    /* Define the user stack in the address space */
    
    result = as_define_stack(as, &stackptr);
    if (result) {
        for(int i = 0; i < args_counter; i ++ ) {
            kfree(args_to_kernel[i]);
        }
        kfree(args_to_kernel);
        return result;
    }
    
    int padded_size = 0;
    /* Calculate total length accounting for padding */
    for (int i = 0 ;i < args_counter; i ++) {
        int arg_len = strlen(args_to_kernel[i]) + 1;
        int padded_item_size = ROUNDUP(arg_len, 4);
        padded_size += (padded_item_size * sizeof(char));
    }
    /* Now we reach the top of the Argument strings */
    stackptr -= padded_size;
    
    /* Copy Arguments string into user stack */
    vaddr_t * argsptr = (vaddr_t *) kmalloc(sizeof(vaddr_t) * (args_counter + 1));
     for (int i = 0 ;i < args_counter; i ++) {
         int arg_len = strlen(args_to_kernel[i]) + 1;
         int padded_item_size = ROUNDUP(arg_len, 4);
         
         /* Update Argument array's address */
         argsptr[i] = stackptr;
         
         /* Copy from a kernel-space address to user-space address */
         copyout((void *) args_to_kernel[i], (userptr_t)stackptr, arg_len);
         
         stackptr += (padded_item_size * sizeof(char));
     }
    argsptr[args_counter] = (vaddr_t)NULL;
    
    /* Now we reach the top of the User stack*/
    stackptr -= padded_size;
    stackptr -= (args_counter + 1) * sizeof(vaddr_t);
    
    /* Copy Argument array into user stack */
    for (int i = 0; i <= args_counter; i++) {
        copyout((void *) (argsptr + i), (userptr_t)stackptr, sizeof(vaddr_t));
        stackptr += sizeof(vaddr_t);
    }
    
    stackptr -= ((args_counter + 1) * sizeof(vaddr_t));
    
    kfree(argsptr);
/* Step 7: Delete old address space. */
    for(int i = 0; i < args_counter; i ++ ) {
        kfree(args_to_kernel[i]);
    }
    kfree(args_to_kernel);
    
    as_destroy(old_address_space);
    
/* Step 8: Call enter_new_process with address to the arguments on the stack, the stack pointer (from as_define_stack), and the program entry point (from vfs_open) */
    /* Warp to user mode. */
    enter_new_process(args_counter, (userptr_t) stackptr, stackptr, entrypoint);
    
/* Enter_new_process does not return. */
    panic("enter_new_process returned\n");
    return EINVAL;
}
#endif
