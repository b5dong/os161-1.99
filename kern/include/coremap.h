#ifndef _COREMAP_H_
#define _COREMAP_H_

#include <spinlock.h>
struct coremap_statue{
    paddr_t paddr;
    paddr_t owner;
    volatile bool inUse;
};

struct coremap{
    struct spinlock coreLock;
    unsigned int Num_Pages;
    struct coremap_statue * status;
};

//struct coremap *init_coremap(void);
static void Page_Release(paddr_t paddr);

#endif

