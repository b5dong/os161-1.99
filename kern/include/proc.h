/*
 * Copyright (c) 2013
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _PROC_H_
#define _PROC_H_

/*
 * Definition of a process.
 *
 * Note: curproc is defined by <current.h>.
 */

#include <addrspace.h>
#include <current.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/errno.h>
#include <kern/wait.h>
#include <mips/trapframe.h>
#include <proc.h>
#include <synch.h>
#include <types.h>
#include <vnode.h>
#include <vfs.h>
#include "opt-A2.h"


struct addrspace;
struct vnode;
#ifdef UW
struct semaphore;
#endif // UW

/*
 * Process structure.
 */

#if OPT_A2
#define __MIN_PID   1
#define __MAX_PID   64
#define __PROCEXITED    1
#define __PROCALIVE    2
#define __DIDNT_EXIT    -1
#endif

struct proc {
	char *p_name;			            /* Name of this process */
	struct spinlock p_lock;		        /* Lock for this structure */
	struct threadarray p_threads;	    /* Threads in this process */
    
	/* VM */
	struct addrspace *p_addrspace;	    /* virtual address space */

	/* VFS */
	struct vnode *p_cwd;		        /* current working directory */

#ifdef UW
  /* a vnode to refer to the console device */
  /* this is a quick-and-dirty way to get console writes working */
  /* you will probably need to change this when implementing file-related
     system calls, since each process will need to keep track of all files
     it has opened, not just the console. */
  struct vnode *console;                /* a vnode for the console device */
#endif

#if OPT_A2
    pid_t pid;
    
    int num_of_child;
    pid_t childPids[__MAX_PID];
    
    struct cv * Table_item_cv;
    
    volatile int procStatus;
    volatile int exitcode;
    volatile int exitstate;
#endif
};

/* This is the process structure for the kernel and for kernel-only threads. */
extern struct proc *kproc;

/* Semaphore used to signal when there are no more processes */
#ifdef UW
extern struct semaphore *no_proc_sem;
#endif // UW

/* Call once during system startup to allocate data structures. */
void proc_bootstrap(void);

/* Create a fresh process for use by runprogram(). */
struct proc *proc_create_runprogram(const char *name);

/* Destroy a process. */
void proc_destroy(struct proc *proc);

/* Attach a thread to a process. Must not already have a process. */
int proc_addthread(struct proc *proc, struct thread *t);

/* Detach a thread from its process. */
void proc_remthread(struct thread *t);

/* Fetch the address space of the current process. */
struct addrspace *curproc_getas(void);

/* Change the address space of the current process, and return the old one. */
struct addrspace *curproc_setas(struct addrspace *);

#if OPT_A2
extern struct proc * Proc_table[__MAX_PID];
extern struct lock * table_lk;
extern int exit_table[__MAX_PID];

pid_t generate_pid(struct proc *proc, int *err);
/* copy the trapframe of parent process and call enter_forked_process */
void forkentry(void *data1, unsigned long data2);

/* get the exit status of process with pid, if not exit, wait */
int Get_exit_Status(pid_t pid, int options);

/* sys call */
void sys__exit(int exitcode);
int sys_getpid(pid_t *retval);
int sys_waitpid(pid_t pid, userptr_t status, int options, pid_t *retval);
int sys_fork(struct trapframe *tf,pid_t *retval);
int sys_execv(char *progname, char **args);
#endif

#endif /* _PROC_H_ */
